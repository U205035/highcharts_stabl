Requirements: 

- Highcharts CSS and JS, Version 6.0.7
- Highmaps, Version 6.0.7
- Used highcharts modules v6.0.7:
    - exporting
    - offline-exporting
    - export-data
- Used open-source highcharts plugin:
    - export-csv, Version 1.4.7
- Swiper CSS and JS, Version 4.3.3
- jQuery, Version 3.3.1




