var OPEN = 0;

function show_div(div, event){
    if(div.style.display  == 'block'){
	div.style.display  = 'none';
    }else{
	var x = 0;
	var y = 0;

	div.style.display  = 'block';
	div.style.position = 'absolue';

	if(event.pageX || event.pageY){
	    x = event.pageX;
	    y = event.pageY;
	}else if(event.clientX || event.clientY){
	    x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
	    y = event.clientY + document.body.scrollTop  + document.documentElement.scrollTop;
	}  
	div.style.display  = 'block';
	div.style.position = 'absolute';
	div.style.zIndex = 100;
	div.style.top = y+"px";
	div.style.left = x+"px";
    }
    OPEN = 0;
}


function init(){
    document.body.onclick = function(){
	if(OPEN == 1 && document.querySelectorAll(".checkbox") > 0){
	    document.querySelectorAll(".checkbox")[0].style.display = "none";
	}else{
	    OPEN = 1;
	}
    }
    document.getElementById("data").scrollTop = 0;
    document.getElementById("data").scrollLeft = 0;
    var ua   = window.navigator.userAgent;
    if(ua.indexOf("MSIE ") > 0){
	IE_CHECK = true;
    }else{
	IE_CHECK = false;	
    }
    var s_height = 2; 
    var i = 0;
    var data_table = document.getElementById("data_table");
    var cell_table = data_table.querySelectorAll('.cell_table')[0];
    i = 0;
    for(var i = 0; i <= cell_table.tHead.rows.length-1; i++){
	var cell = cell_table.tHead.rows[i].cells[1];
        s_height += cell.offsetHeight;
    }

    head_row_1 = document.getElementById('head').children[0].tHead.firstElementChild;

    document.getElementById("head").style.height  = s_height + "px"

    data_table.style.width = data_table.offsetWidth+20+data_table.offsetWidth-data_table.clientWidth + "px";
    document.getElementById('head').style.width = data_table.style.width;
 
    var data = document.getElementById("data");
    if(window.innerHeight < document.body.offsetHeight-200){
	data.style.height = 0+'px';
	count = 0
	
	while(window.innerHeight-30 > document.body.offsetHeight){
	    data.style.height = data.offsetHeight+window.innerHeight-document.body.offsetHeight-30+'px';
	    if(count >= 80) break;
	    count++;
	}
	if(data.offsetHeight > cell_table.offsetHeight){
	    data.style.height = cell_table.offsetHeight+'px'; 
	}
	if(data.offsetHeight < 120){
	    data.style.height = '120px'; 
	}
    }
}  

window.onresize = function(){
    var data_table = document.getElementById("data_table");
	
    var data = document.getElementById("data");
    var cell_table = data_table.querySelectorAll('.cell_table')[0];
    if(window.innerHeight < document.body.offsetHeight-200){
	data.style.height = 0+'px';
	count = 0;	
	while(window.innerHeight-30 > document.body.offsetHeight){
	    data.style.height = data.offsetHeight+window.innerHeight-document.body.offsetHeight-30+'px';
	    if(count >= 80) break;
	    count++;
	}
	/*if(data.offsetHeight > cell_table.offsetHeight){
	    data.style.height = cell_table.offsetHeight+'px'; 
	}*/

    }/*else{
	data.style.height = cell_table.offsetHeight+'px';
    }*/
    if(data.offsetHeight < 120){
	data.style.height = '120px'; 
    }
};
