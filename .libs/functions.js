//function to read a specific url-parameter
//source: https://www.jaqe.de/2009/01/16/url-parameter-mit-javascript-auslesen/
function get_url_param(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

//function to capitalize first letter
//source: https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript#1026087
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//ajax-call function
function ajax_call(dataType,
    url,
    success,
    call,
    errorMsg,
    counterBoolean) {
    return $.ajax({
        type: 'GET',
        dataType: dataType,
        url: dynamic_url() + 'daten/' + url,
        success: success,
        error: function (jqxhr, status, error) {
            console.log(call + ' ajax-call: error status: ' + status + '\n' +
                call + ' ajax-call: error message: ' + error);
            $('.errorDiv').html(errorMsg);

            /*
             * special function - 
             * used if csv-file doesn't exist. It counts +1 for the karussell
             */
            if (counterBoolean != undefined) {
                counter++;
            }
        }
    });
}

//sets the url dynamically until the root folder 'highmaps'
function dynamic_url() {
    var origin = window.location.origin;
    var arrayOfPath = window.location.pathname.split('/');
    var index = arrayOfPath.indexOf('highmaps');
    var path = '';

    for (var i = 0; i < index; i++) {
        path += arrayOfPath[i] + '/';
    }

    return origin + path + 'highmaps/';
}

//sets the gebiet variable
function set_gebiet_variable() {
    var gebiet;
    if (window.location.href.indexOf('gemeinde') > -1) { //check if the request comes from the 'region','gemeinde' or 'kanton'
        gebiet = 'gemeinde';
    } else if (window.location.href.indexOf('region') > -1) {
        gebiet = 'region';
    } else if (window.location.href.indexOf('kanton') > -1) {
        gebiet = 'kanton';
    } else if (window.location.href.indexOf('punkt') > -1) {
        gebiet = 'punkt';
    }
    return gebiet;
}

//checks the 'thema' value from url and optionally also the "variante" value (logik.js)
function check_param_values_with_themenliste(checkVarianteToo, callback) {
    return ajax_call('json', 'liste/themenliste.json', function (json, status) {
        $.each(json.themen, function (index, value) {
            if (get_url_param('thema') == value.thema) {
                createMap = true;
                themenname = value.thema;

                if (checkVarianteToo) {
                    for (var i = 0; i < value.variante.length; i++) {
                        variantenarray.push(value.variante[i].name);

                        if (get_url_param('variante') == value.variante[i].name) {
                            variantenname = value.variante[i].name;
                        }
                    }
                }
            }
        });

        if (get_url_param('variante') == '') { //no value for key 'variante'
            variantenname = 'alle'; //you get all variantes for this topic
        }

        if (themenname == undefined) { //themenname doesn't exist - error Message will be displayed
            error = 'Der Wert des <b>Thema</b> - Parameters existiert nicht oder wurde nicht angegeben! Bitte geben Sie einen korrekten Wert an.';
            document.write(error);
        }

        if (typeof (callback) == 'function') { //check if callback is function
            callback();
        }
    }, 'themenliste.json - ajax-call: ', 'Beim Aufruf der Themenliste.json - Datei ist ein Fehler aufgetreten.');
}

/*
 * checks the gebiet (gemeinde/region/kanton/punkt)
 */
function check_gebiet_value_with_gebietliste(callback) {
    return ajax_call('json',
        'liste/' + gebiet + 'liste.json',
        function (json, status) {

            /*
             * special treatment than the others gebietslisten
             * punktliste needs also the themenname 
             */
            $.each(json.gebiet,
                function (index, value) {
                    if (gebiet != 'punkt') {
                        gebietsarray.push(value.name); //get all the names in an array
                        if (get_url_param(gebiet) == value.id) {
                            gebietsname = value.name;
                        }
                    } else {
                        $.each(value,
                            function (i, val) {
                                themenarray.push(i);
                                if (i == themenname) { //checks if the themenname exists
                                    $.each(val,
                                        function (number, item) {
                                            gebietsarray.push({
                                                "id": item.id,
                                                "name": item.name
                                            });
                                            if (get_url_param(gebiet) == item.id) {
                                                gebietsname = item.name;
                                            }
                                        });
                                }
                            });
                    }
                });

            if (typeof (callback) == 'function') { //used in karte.htm
                callback();
            }

            if (get_url_param(gebiet) == '') { //checks if no value for the gebiet was entered
                gebietsname = 'alle';
            }
        }, gebiet + 'liste.json - ajax-call: ',
        'Beim Aufruf der ' + gebiet + 'liste.json - Datei ist ein Fehler aufgetreten.');
}
