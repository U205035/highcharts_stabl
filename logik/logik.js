var variante = 'variante',
    thema = 'thema',
    gebiet,
    variantenname,
    themenname,
    gebietsname,
    variantenarray = [],
    gebietsarray = [],
    credits,
    counter = 0,
    error,
    numberOfChartsForSwiper,
    createCharts = true,
    themenarray = [];

$(document).ready(function () {
    check_thema_key_value();
});

/* 
 * function to check param key and values from 'thema'
 * It is separated on purpose from the other parameters because if this isn't declared
 * the 'Zahlenfenster' can't be displayed (highlighted) on the right thema e.g. 'Wasserstatistik'
 */
function check_thema_key_value() {
    if (window.location.href.indexOf('thema') == -1) { //thema key parameter doesn't exist
        error = 'Der Themen - Parameter wurde nicht angegeben.';
        document.write(error);
    } else {
        check_param_values_with_themenliste(true,
            execute_checks_and_start_separation_of_charts); //check thema value parameter and callback function for the other checks
    }
}

/* 
 * executes all the parameter checks and  
 * handles the different options for the parameters
 * Starts the separations of the csv function
 */
function execute_checks_and_start_separation_of_charts() {
    if (check_existence_of_parameters_gebiet_variante()) { //if false the charts are not created and errorMsg will be displayed
        $.when(gebiet = set_gebiet_variable(),
            check_gebiet_value_with_gebietliste()).done(function () { //callbacks through jquery - "done" is executed when "when" is finished
            if (check_values_of_parameters_gebiet_and_variante()) { //checks if the values of the parameters exist

                var urlBeginning = thema + '/' +
                    themenname + '/' +
                    gebiet + '/';

                /*
                 * Variante 2 - one chart over all gemeinde
                 */

                if (gebietsname == 'alle' && variantenname != 'alle') {

                    numberOfChartsForSwiper = gebietsarray.length;
                    for (var i in gebietsarray) {
                        if(window.location.href.indexOf('punkt') > -1) {
                            gebietsname = gebietsarray[i].name;
                            separate_csv_data(urlBeginning + gebietsarray[i].name + '/' + variantenname + '.csv',
                                variantenname,
                                i);                            
                        } else {
                            gebietsname = gebietsarray[i];
                            separate_csv_data(urlBeginning + gebietsarray[i] + '/' + variantenname + '.csv',
                                variantenname,
                                i);                            
                        }
                    }

                    /*
                     * Variante 3 - synchronized chart with all charts for one gemeinde
                     */

                } else if (gebietsname != 'alle' && variantenname == 'alle') {

                    numberOfChartsForSwiper = variantenarray.length;
                    $.each(variantenarray,
                        function (index, value) {
                            separate_csv_data(urlBeginning +
                                gebietsname + '/' +
                                value + '.csv',
                                value,
                                index);
                        });

                    /* 
                     * Variante 1 - one chart for one gemeinde
                     */

                } else if (gebietsname != 'alle' && variantenname != 'alle') {

                    numberOfChartsForSwiper = 1;
                    separate_csv_data(urlBeginning +
                        gebietsname + '/' +
                        variantenname + '.csv',
                        variantenname,
                        0);

                } else {
                    error = 'Die Werte des <b>Varianten</b> - und des <b>' + capitalizeFirstLetter(gebiet) + '</b> - Parameters wurden nicht angegeben!';
                }
            }
            /** Gefällt mir noch nicht so gut **/
            $('#container').html(error);
        });
    }
    /** Gefällt mir noch nicht so gut **/
    $('#container').html(error);
}

/*
 * checks if gebiet and variante parameter exist
 */
function check_existence_of_parameters_gebiet_variante() {
    if (window.location.href.indexOf('gemeinde') > -1 ||
        window.location.href.indexOf('kanton') > -1 ||
        window.location.href.indexOf('region') > -1 ||
        window.location.href.indexOf('punkt') > -1) {} else { //checks if one of them exist and if not there will be an error
        error = 'Der Gebiet - Parameter wurde nicht angegeben.';
        createCharts = false;
    }

    if (window.location.href.indexOf('variante') == -1) { //if url parameter 'variante' doesn't exist = error Message
        createCharts = false;
        error = 'Der Varianten - Parameter wurde nicht angegeben.';
    }
    return createCharts;
}

/*
 * Checks the values of the parameters and 
 * if they don't exist there will be an error message displayed
 */
function check_values_of_parameters_gebiet_and_variante() {
    if (gebietsname == undefined) {
        error = 'Der Wert des <b>' +
            capitalizeFirstLetter(gebiet) +
            '</b> - Parameters existiert nicht oder wurde nicht angegeben!';
        createCharts = false;
    }
    if (variantenname == undefined) {
        error = 'Der Wert des <b>Variante</b> -' +
            'Parameters existiert nicht oder wurde nicht angegeben!';
        createCharts = false;
    }

    return createCharts;
}

/* 
 * separates the csv data so it can be used with highcharts correctly
 * starts the create_chart() function
 */
function separate_csv_data(url, variante, chartPosition) {
    //ajax-call to receive the data from the csv-file
    ajax_call('text',
        url,
        function (csv, status) {
            /*
             * this if statement checks the existence of the swiper key
             * if it exists the swiper will be used
             * if it doesn't exist : results that the charts are on top of each other
             */
            if (window.location.href.indexOf('swiper') > -1) {
                $('<div class="swiper-slide" id="' + chartPosition + '">')
                    .appendTo('.swiper-wrapper'); //slide-div
            } else {
                $('<div id="' + chartPosition + '">')
                    .appendTo('#container');
                $('.swiper-container').hide();
            }

            //splits the csv lines into arrays
            $.csv.toArrays(csv, {
                separator: ';'
            }, function (err, csv) {

                // only log this msg if an error occurred
                if (err) {
                    console.log('csv-to-Arrays-error: ' + err);
                }

                // array to store arrays with the data for the charts
                var infos = [],
                    titles = [],
                    series = [],
                    title;

                // creates the specific amount of arrays which is needed to store the data 
                for (var i = 0; i < csv[0].length; i++) {
                    infos[i] = [];
                }

                $.each(csv,
                    function (index, value) { //for-each-loop through every line
                        for (var i = 0; i < value.length; i++) { //for-loop through the different data pieces of each line
                            if (index > 1) { //the first two lines will be omitted
                                infos[i].push(Number(value.slice(i, i + 1))); //splices the array. Adds from each line the same slot to the same array.
                            } else if (index == 1) {
                                titles.push(value[i]); //add the titles of the chart to the 'titles' array
                            } else if (index == 0) {
                                if (value[0].indexOf('m3') > -1) {
                                    value[0] = value[0].replace('m3', 'm\u00B3');
                                }
                                title = value[0];
                            }
                        }
                    });

                for (var i = 0; i < csv[0].length - 1; i++) { //the first column of each csv is the year-value - We dont' want this value to be safed in series : thats why 'csv[0].length - 1' and 'infos[i + 1]'
                    series[i] = { //Adds the title + the data to the series array
                        name: titles[i + 1],
                        data: infos[i + 1]
                    };
                }

                //loads the specific .json-file of one single variante and creates the chart/s. if it doesn't exist the options.json will be loaded
                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: dynamic_url() +
                        'daten/thema/' +
                        themenname +
                        '/json/' +
                        variante +
                        '.json',
                    success: function (json, status) {
                        create_chart(json,
                            title,
                            infos,
                            series,
                            chartPosition);
                    },
                    error: function (jqxhr, error, status) {

                        console.log('warning-status: ' + status + '\n' +
                            'options.json was loaded instead of ' +
                            variante + '.json');

                        ajax_call('json',
                            'thema/' + themenname + '/json/options.json',
                            function (json, status) {
                                create_chart(json,
                                    title,
                                    infos,
                                    series,
                                    chartPosition);
                            }, 'options.json',
                            'Beim Aufruf der options.json - Datei ist ein Fehler aufgetreten.');
                    },
                });
            });


        }, variante + '.csv für ' + gebietsname + ' - load charts - ',
        'Beim Aufruf der CSV-Datei ' + variante + '.csv für ' + gebietsname + ' ist ein Fehler aufgetreten.', true); //true is for the counterBoolean.
}

/* 
 * edits some options for the charts and creates it afterwards with highcharts method
 */
function create_chart(json, title, infos, series, chartPosition) {

    //if there is a special subtitle defined in json
    if (json.subtitle != undefined) {
        if (json.subtitle.special != undefined) {
            for (var i = 0; i < json.subtitle.special.length; i++) { //checks if the gebietsname exists for specialized subtitles
                if (gebietsname == json.subtitle.special[i].gemeinde) {
                    subtitle = json.subtitle.special[i].text;
                }
            }
        }
    }

    //these variables are from the csv file
    json.series = series;
    json.title.text = title;
    json.xAxis.categories = infos[0];

    // superscripted number for subtitle
    if (json.subtitle.text != '') {
        json.subtitle.text += '\u00B9';
        json.title.text += '\u00B9';
    }

    // charts 
    $('<div class="chart">').appendTo('#' + chartPosition).highcharts(json); //create chart with loaded json options
    counter++; //counts up 
    if (counter == numberOfChartsForSwiper) { //if all charts are created the swiper will be created
        create_swiper();
    }
}

/*
 * creates the swiper for the karussell
 */
function create_swiper()  {
    var mySwiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true, //when there are more than 7 bullets, it adds them dynamically at change
            dynamicMainBullets: 7,
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        keyboard: {
            enabled: true,
            onlyInViewport: false
        }
    });
}
