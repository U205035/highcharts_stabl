$(document).ready(function(){
    $('#kanton').attr('href', dynamic_url() + 'logik/zahlenfenster.htm?kanton');
    $('#region').attr('href', dynamic_url() + 'logik/zahlenfenster.htm?region');
    $('#gemeinde').attr('href', dynamic_url() + 'logik/zahlenfenster.htm?gemeinde');
    $('#punkt').attr('href', dynamic_url() + 'logik/zahlenfenster.htm?punkt');
    
    var urlParam = window.location.search;
    
    if(urlParam == "?kanton") {
        $('#kanton').addClass('selected');
        $('#embed').attr('src', dynamic_url() + 'logik/logik.htm?thema=wasserstatistik&kanton=100&variante=&swiper');
    } else if(urlParam == "?region") {
        $('#region').addClass('selected');
        $('#embed').attr('src', dynamic_url() + 'logik/karte.htm?thema=wasserstatistik&gebiet=region');
    } else if(urlParam == "?gemeinde") {
        $('#gemeinde').addClass('selected');
        $('#embed').attr('src', dynamic_url() + 'logik/karte.htm?thema=wasserstatistik&gebiet=gemeinde');
    } else if(urlParam == "?punkt") {
        $('#punkt').addClass('selected');
        $('#embed').attr('src', dynamic_url() + 'logik/karte.htm?thema=wasserwerk&gebiet=punkt');
    } else {
        document.write('Es muss einer der folgenden URL-Parameter mitgegeben werden: region, kanton, gemeinde oder punkt.');
    }
    
});