var createMap = false,
    error = 'Der Wert des <b>Thema</b> - Parameters existiert nicht oder wurde nicht angegeben! Bitte geben Sie einen korrekten Wert an.',
    gebietParamOk,
    gebiet,
    themenname,
    themenarray = [],
    gebietsarray = [];

$(document).ready(function () {
    if (window.location.href.indexOf('thema') > -1 && 
        window.location.href.indexOf('gebiet') > -1) { //checks if the parameters exist
        gebiet = set_gebiet_variable(); //function is located in .libs/functions.js

        $.when(check_param_values_with_themenliste(false, '')).done(function () { //checks the thema parameter value

            if (createMap) { //variable gets changed in .libs/functions.js check_param_values_with_themenliste !!!

                if (check_gebiet_param_value()) { //checks the gebiet parameter value   

                    if (gebiet != 'punkt') {
                        ajax_call('json', 'karte/' + gebiet + '.geojson', function (geojson, status) {

                            var karte = Highcharts.geojson(geojson, 'map'); //restructs the geojson to fit with the series.mapData option.
                            Highcharts.mapChart('container', { //creates the mapChart
                                chart: {},
                                credits: {
                                    href: '',
                                    text: '',
                                    position: {
                                        align: 'left',
                                        x: 10,
                                        y: -5
                                    }
                                },
                                mapNavigation: {
                                    enabled: false
                                },
                                plotOptions: { //options for the data in 'series'
                                    series: {
                                        point: {
                                            events: {
                                                select: function () { //opens the site with the charts for the clicked 'gemeinde' / 'region'
                                                    var id;
                                                    if (gebiet == 'gemeinde') { //sets the right variable for the geojson id of the gebiet
                                                        id = this.properties.gemeinde_id_bfs;
                                                    } else {
                                                        id = this.properties.nummer;
                                                    }
                                                    window.open(dynamic_url() + 'logik/logik.htm?thema=' + get_url_param('thema') + '&variante=&' + gebiet + '=' + id + '&swiper', '_self'); //opens the charts for the clicked gebiet
                                                }
                                            }
                                        }
                                    }
                                },
                                series: [{ //options for the geojson data
                                    data: karte,
                                    name: 'BL',
                                    allowPointSelect: true
                            }],
                                subtitle: {

                                },
                                exporting: {
                                    filename: 'Abbildung_Amt_fuer_Geoinformation',
                                    enabled: false
                                },
                                title: {
                                    align: 'center',
                                    text: 'Bitte ' + capitalizeFirstLetter(gebiet) + ' auswählen'
                                },
                                legend: {
                                    enabled: false
                                },
                                tooltip: {
                                    pointFormat: '{point.properties.name}'
                                }
                            });
                        }, gebiet + ' .geojson', 
                                  'Beim Aufruf der ' + gebiet + '.geojson - Datei ist ein Fehler aufgetreten.');

                    } else {
                        $('#container').append('<div class="dropdown">' +
                            '<button onclick="dropdownFunction()" class="dropBtn">' +
                            capitalizeFirstLetter(get_url_param('thema')) + ' auswählen</button>' +
                            '<div id="myDropdown" class="dropdown-content">' +
                            '</div>' +
                            '</div>');

                        check_gebiet_value_with_gebietliste(setDropdownProps);
                    }
                } else {
                    $('#container').html(error);
                }
            } else {
                document.write(error);
            }
        });
    } else {
        document.write('Der <b>Thema</b> und/oder <b>Gebiet</b> - Parameter wurde nicht angegeben.');
    }
});


//checks if the entered gebiets parameter value is 'region' or 'gemeinde' 
function check_gebiet_param_value() {
    if (gebiet == 'gemeinde' || gebiet == 'region' || gebiet == 'punkt') {
        gebietParamOk = true;
    } else {
        gebietParamOk = false;
        error = 'Es sind nur die Werte <b>region</b>, <b>gemeinde</b> oder <b>punkt</b> für den Gebiets-Parameter erlaubt.';
    }
    return gebietParamOk;
}

//function for the dropdownmenu - switches between show and hide for the menu
function dropdownFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function setDropdownProps() {
    for (var i in gebietsarray) {
        $('#myDropdown').append('<a href="' + dynamic_url() + 'logik/logik.htm?thema=' + get_url_param('thema') + '&variante=&punkt=' + gebietsarray[i].id + '">' + gebietsarray[i].name + '</a>');
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropBtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
}
